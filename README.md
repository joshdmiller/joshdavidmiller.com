This is the source code for [joshdavidmiller.com](http://joshdavidmiller.com) and is made available
under the terms of the [MIT
license](http://github.com/joshdmiller/joshdavidmiller.com/blob/master/LICENSE).

