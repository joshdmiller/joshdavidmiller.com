angular.module 'jdm/sections/blog', [
  'ngRoute'
]

  .config ( $routeProvider ) ->
    $routeProvider.when '/articles',
      controller: 'BlogCtrl'
      templateUrl: '@tpl/sections/blog/blog.tpl.html'

  .controller 'BlogCtrl', ( $scope ) ->
    console.log "Blog controller"



