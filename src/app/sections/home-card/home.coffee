angular.module 'jdm/sections/home', [
  'ngRoute'
]

  .config ( $routeProvider ) ->
    $routeProvider.when '/',
      controller: 'HomeCtrl'
      templateUrl: '@tpl/sections/home-card/home.tpl.html'

    # $routeProvider.otherwise { redirectTo: '/' }

  # .run ( _sectionManager ) ->
  #   _sectionManager.add 0, 'home', '@tpl/sections/home-card/home.tpl.html'

  .controller 'HomeCtrl', ( $scope ) ->
    console.log "Home controller"

