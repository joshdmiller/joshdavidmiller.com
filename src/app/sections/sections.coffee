angular.module 'jdm/sections', [
  'jdm/sections/home'
  'jdm/sections/contact'
  'jdm/sections/blog'
]

  .factory '_sectionManager', () ->
    sections = []

    _sectionManager =
      add: ( num, name, tpl ) ->
        sections.push { num: num, name: name, tpl: tpl }

      get: () ->
        sections
          .sort ( a, b ) ->
            if a.num < b.num
              -1
            else if a.num > b.num
              1
            else
              0

  .controller 'SectionsCtrl', ( $scope, _sectionManager ) ->
    $scope.sections = _sectionManager.get()

  .directive 'sections', () ->
    sections =
      controller: 'SectionsCtrl'
      templateUrl: '@tpl/sections/sections.tpl.html'

