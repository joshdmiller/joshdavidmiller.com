angular.module 'jdm/sections/contact', [
  'ngRoute'
]

  .config ( $routeProvider ) ->
    $routeProvider.when '/contact',
      controller: 'ContactCtrl'
      templateUrl: '@tpl/sections/contact-card/contact.tpl.html'

  .run ( _sectionManager ) ->
    _sectionManager.add 10, 'contact', '@tpl/sections/contact-card/contact.tpl.html'

  .controller 'ContactCtrl', ( $scope ) ->
    console.log "Contact controller"


