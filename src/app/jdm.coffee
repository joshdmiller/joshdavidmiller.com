angular.module 'jdm', [
  'ngRoute'
  'templates'
  'jdm/sections'
  'jdm/menu'
]

  .controller 'JdmCtrl', ( $scope ) ->
    $scope.menuOpen = false
    $scope.toggleMenu = () ->
      $scope.menuOpen = if $scope.menuOpen then false else true

