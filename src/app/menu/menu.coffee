angular.module 'jdm/menu', []

  .directive 'slidingMenu', ( $timeout, $anchorScroll, $location ) ->
    slidingMenu =
      scope:
        in: '=slidingMenu'
      templateUrl: '@tpl/menu/menu.tpl.html'
      link: ( scope, element, attrs ) ->
        scope.$watch 'in', ( open ) ->
          return if not open?

          if open
            show()
          else
            hide()

        show = scope.show = () ->
          # FIXME: replace with ng-animate
          element.children().css 'z-index', '1'
          scope.in = true

        hide = scope.hide = () ->
          console.log "hiding"
          # FIXME: replace with ng-animate
          $timeout () ->
            element.children().css 'z-index', '-1'
          , 400

          scope.in = false

        scope.go = ( path ) ->
          hide()
          $location.path "/#{path}"
          $anchorScroll()

